package pl.gda.pg.commons.json.parser;

import io.vertx.core.json.JsonObject;
import pl.gda.pg.commons.json.Predicates;

import java.lang.reflect.Field;

public class SimpleFieldProcessor {

	public void processSimpleData(JsonObject json, Field field, Object objectToSet) throws IllegalAccessException {
		Class<?> type = field.getType();
		if (Predicates.isSimpleField(type)) {
			Object value = json.getValue(field.getName());
			field.set(objectToSet, value);
		}
	}
}
