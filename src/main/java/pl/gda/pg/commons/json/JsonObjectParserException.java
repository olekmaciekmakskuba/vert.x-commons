package pl.gda.pg.commons.json;

public class JsonObjectParserException extends RuntimeException {

	public JsonObjectParserException(String message) {
		super(message);
	}
}
