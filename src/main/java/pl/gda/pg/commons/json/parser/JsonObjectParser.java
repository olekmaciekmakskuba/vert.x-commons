package pl.gda.pg.commons.json.parser;

import io.vertx.core.json.JsonObject;
import pl.gda.pg.commons.json.JsonObjectParserException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class JsonObjectParser {

	private final static JsonObjectParser INSTANCE = new JsonObjectParser();

	public static JsonObjectParser getInstance() {
		return INSTANCE;
	}

	private SimpleFieldProcessor simpleField;
	private ObjectFieldProcessor objectField;
	private ListFieldProcessor listField;

	private JsonObjectParser() {
		simpleField = new SimpleFieldProcessor();
		objectField = new ObjectFieldProcessor();
		listField = new ListFieldProcessor();
	}

	public <T> T convertJson(JsonObject json, Class<T> clazz) {
		try {
			T object = clazz.newInstance();

			for (Field field : clazz.getDeclaredFields()) {

				field.setAccessible(true);

				simpleField.processSimpleData(json, field, object);
				listField.processList(json, field, object);
				objectField.processObjectField(json, field, object);

			}
			return object;
		} catch (IllegalAccessException | NoSuchMethodException
				| InstantiationException | InvocationTargetException
				| ClassNotFoundException | IllegalArgumentException e) {
			e.printStackTrace();
			throw new JsonObjectParserException("We can't do this!");
		}
	}
}
