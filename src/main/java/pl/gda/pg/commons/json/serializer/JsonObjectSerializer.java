package pl.gda.pg.commons.json.serializer;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import pl.gda.pg.commons.json.Predicates;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

public class JsonObjectSerializer {

	private static final JsonObjectSerializer INSTANCE = new JsonObjectSerializer();

	public static JsonObjectSerializer getInstance() {
		return INSTANCE;
	}

	private JsonObjectSerializer() {
	}

	public JsonObject serializeObject(Object object) {
		JsonObject json = new JsonObject();

		for (Field field : object.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				Object fieldObj = field.get(object);
				String fieldName = field.getName();

				processSimpleType(fieldObj).ifPresent(obj -> json.put(fieldName, obj));
				processListType(fieldObj).ifPresent(obj -> json.put(fieldName, obj));
				processObjectType(fieldObj).ifPresent(obj -> json.put(fieldName, obj));

			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return json;
	}

	private Optional<JsonArray> processListType(Object object) {
		Class<?> type = object.getClass();
		if (Predicates.isList(type)) {
			JsonArray array = new JsonArray();
			List<?> list = (List) object;
			list.stream().map(this::serializeObject).forEach(array::add);
			return Optional.of(array);
		}
		return Optional.empty();
	}

	private Optional<Object> processSimpleType(Object object) {
		Class<?> type = object.getClass();
		if (Predicates.isSimpleField(type)) {
			return Optional.of(object);
		}
		return Optional.empty();
	}

	private Optional<JsonObject> processObjectType(Object object) {
		Class<?> type = object.getClass();
		if (Predicates.isComplexObject(type)) {
			JsonObject childObj = serializeObject(object);
			return Optional.of(childObj);
		}
		return Optional.empty();
	}
}
