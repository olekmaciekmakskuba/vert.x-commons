package pl.gda.pg.commons.json;

import org.apache.commons.lang3.ClassUtils;

import java.util.List;
import java.util.function.Predicate;

public class Predicates {

	private static Predicate<Class<?>> isPrimitiveTypeOrWrapper = ClassUtils::isPrimitiveOrWrapper;
	private static Predicate<Class<?>> isString = String.class::isAssignableFrom;
	private static Predicate<Class<?>> isList = List.class::isAssignableFrom;

	public static boolean isComplexObject(Class<?> type) {
		return isPrimitiveTypeOrWrapper.or(isString).or(isList).negate().test(type);
	}

	public static boolean isSimpleField(Class<?> type) {
		return isPrimitiveTypeOrWrapper.or(isString).test(type);
	}

	public static boolean isList(Class<?> type) {
		return isList.test(type);
	}
}
