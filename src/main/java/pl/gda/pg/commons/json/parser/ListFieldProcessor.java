package pl.gda.pg.commons.json.parser;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ListFieldProcessor {

	public void processList(JsonObject json, Field field, Object objectToSet)
			throws IllegalAccessException, ClassNotFoundException,
			NoSuchMethodException, SecurityException, InstantiationException,
			IllegalArgumentException, InvocationTargetException {

		if (field.getType() == List.class) {

			Class<?> genericClass = getGenericClass(field);

			JsonArray jsonArray = json.getJsonArray(field.getName());
			List<JsonObject> jsonsInArray = mapArrayToList(jsonArray);

			List<Object> list = getDataFromJsonObjects(genericClass, jsonsInArray);
			field.set(objectToSet, list);
		}
	}

	private List<JsonObject> mapArrayToList(JsonArray jsonArray) {
		int length = jsonArray.size();
		return IntStream.range(0, length)
						.mapToObj(jsonArray::getJsonObject)
						.collect(Collectors.toList());
	}

	private List<Object> getDataFromJsonObjects(Class<?> type, List<JsonObject> jsonsInArray) {
		return jsonsInArray.stream()
						   .map(obj -> getInstanceOptionally(type, obj))
						   .collect(Collectors.toList());
	}

	private Object getInstanceOptionally(Class<?> type, JsonObject obj) {
		return JsonObjectParser.getInstance().convertJson(obj, type);
	}

	private Class<?> getGenericClass(Field field) throws ClassNotFoundException {
		ParameterizedType genericType = (ParameterizedType) field.getGenericType();
		String genericTypeName = genericType.getActualTypeArguments()[0].getTypeName();
		return Class.forName(genericTypeName);
	}
}
