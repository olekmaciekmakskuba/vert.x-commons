package pl.gda.pg.commons.json.parser;

import io.vertx.core.json.JsonObject;
import pl.gda.pg.commons.json.Predicates;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class ObjectFieldProcessor {

	public void processObjectField(JsonObject json, Field field,
			Object objectToSet) throws NoSuchMethodException,
			IllegalAccessException, InvocationTargetException,
			InstantiationException {

		Class<?> type = field.getType();
		if (Predicates.isComplexObject(type)) {
			Object object = JsonObjectParser.getInstance().convertJson(
					json.getJsonObject(field.getName()), type);
			field.set(objectToSet, object);
		}
	}
}
