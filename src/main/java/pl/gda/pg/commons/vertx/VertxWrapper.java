package pl.gda.pg.commons.vertx;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import pl.gda.pg.commons.event.bus.wrapper.EventBusWrapper;

public class VertxWrapper {

	private final EventBusWrapper eb;
	private final Vertx vertx;

	public VertxWrapper(Vertx vertx) {
		this.vertx = vertx;
		this.eb = new EventBusWrapper(vertx.eventBus());
	}

	public Vertx vertx() {
		return vertx;
	}

	public EventBusWrapper eventBusWrapper() {
		return eb;
	}

	public EventBus eventBus() {
		return vertx.eventBus();
	}
}
