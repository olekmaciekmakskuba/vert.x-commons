package pl.gda.pg.commons.event.bus.wrapper;

@FunctionalInterface
public interface MessageHandler<T> {
	void handle(T t);
}
