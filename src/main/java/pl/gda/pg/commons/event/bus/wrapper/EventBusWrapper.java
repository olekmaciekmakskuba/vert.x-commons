package pl.gda.pg.commons.event.bus.wrapper;

import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import pl.gda.pg.commons.json.parser.JsonObjectParser;
import pl.gda.pg.commons.json.serializer.JsonObjectSerializer;

public class EventBusWrapper {

	private JsonObjectParser parser = JsonObjectParser.getInstance();
	private JsonObjectSerializer serializer = JsonObjectSerializer.getInstance();

	private final EventBus eb;

	public EventBusWrapper(EventBus eb) {
		this.eb = eb;
	}

	public <T> void consumer(String address, MessageHandler<T> handler, Class<T> clazz) {
		System.out.println("consumer address: " + address);
		eb.consumer(address, event -> {
					System.out.print("receive message!");
					JsonObject object = (JsonObject) event.body();
					T t = parser.convertJson(object, clazz);
					handler.handle(t);
				}
		);
	}

	public <T> void send(String address, T data) {
		JsonObject json = serializer.serializeObject(data);
		eb.send(address, json);
	}

	public <T> void publish(String address, T data) {
		JsonObject json = serializer.serializeObject(data);
		eb.publish(address, json);
	}
}
