package pl.gda.pg.commons.json.serializer;

import io.vertx.core.json.JsonObject;
import org.junit.Test;
import pl.gda.pg.commons.json.parser.JsonObjectParser;
import pl.gda.pg.commons.json.testdata.ProjectData;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class JavaObjectSerializerTest {

	private JsonObjectSerializer testObj =JsonObjectSerializer.getInstance();
	private String jsonString = "{\"classes\":[{\"packageName\":\"sample\",\"fileName\":\"Math.java\",\"content\":\"cGFja2FnZSBzYW1wbGU7DQoNCnB1YmxpYyBjbGFzcyBNYXRoIHsNCgkNCglwdWJsaWMgZG91YmxlIGFkZChkb3VibGUgZmlyc3QsIGRvdWJsZSBzZWNvbmQpewkJDQoJCXJldHVybiBmaXJzdCtzZWNvbmQ7DQoJfQ0KfQ==\"}],\"tests\":[{\"packageName\":\"sample\",\"fileName\":\"MathTest.java\",\"content\":\"cGFja2FnZSBzYW1wbGU7DQoNCmltcG9ydCBvcmcuanVuaXQuVGVzdDsNCg0KaW1wb3J0IHN0YXRpYyBvcmcuanVuaXQuQXNzZXJ0Lio7DQoNCnB1YmxpYyBjbGFzcyBNYXRoVGVzdCB7DQoNCglwcml2YXRlIE1hdGggbWF0aCA9IG5ldyBNYXRoKCk7DQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCBzaG91bGRBZGROdW1iZXJzKCl7DQoJCS8vIGdpdmVuDQoJCWludCBhID0gMTA7DQoJCWRvdWJsZSBiID0gMi41Ow0KCQkNCgkJLy8gd2hlbg0KCQlkb3VibGUgcmVzdWx0ID0gbWF0aC5hZGQoYSwgYik7DQoJCQ0KCQkvLyB0aGVuDQoJCWFzc2VydEVxdWFscyhyZXN1bHQsIDEyLjUsIDAuMDAxKTsJCQ0KCX0JDQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCB0aGlzVGVzdFNob3VsZEZhaWxlZCgpew0KCQlhc3NlcnRGYWxzZSgiYmxhYmxhIix0cnVlKTsNCgl9DQp9DQo=\"}],\"buildGradle\":{\"content\":\"YXBwbHkgcGx1Z2luOiAnamF2YScNCg0Kc291cmNlQ29tcGF0aWJpbGl0eSA9IDEuNw0KdmVyc2lvbiA9ICcxLjAnDQoNCnJlcG9zaXRvcmllcyB7DQogICAgbWF2ZW5DZW50cmFsKCkNCn0NCg0KZGVwZW5kZW5jaWVzIHsNCg0KICAgIHRlc3RDb21waWxlIGdyb3VwOiAnanVuaXQnLCBuYW1lOiAnanVuaXQnLCB2ZXJzaW9uOiAnNC4xMicNCn0=\"},\"submissionId\":110}";

	@Test
	public void shouldSerializeToJson() throws IllegalAccessException, InstantiationException {
		// given
		ProjectData data = JsonObjectParser.getInstance().convertJson(new JsonObject(jsonString), ProjectData.class);

		// when
		JsonObject jsonObject = testObj.serializeObject(data);

		// then
		assertThat(jsonObject.encode(), is(jsonString));
	}
}
