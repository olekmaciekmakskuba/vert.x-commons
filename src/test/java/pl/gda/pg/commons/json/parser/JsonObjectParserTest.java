package pl.gda.pg.commons.json.parser;

import io.vertx.core.json.JsonObject;
import org.junit.Test;
import pl.gda.pg.commons.json.JsonObjectParserException;
import pl.gda.pg.commons.json.testdata.FileData;
import pl.gda.pg.commons.json.testdata.ProjectData;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class JsonObjectParserTest {

	private String jsonString = "{\"classes\":[{\"packageName\":\"sample\",\"fileName\":\"Math.java\",\"content\":\"cGFja2FnZSBzYW1wbGU7DQoNCnB1YmxpYyBjbGFzcyBNYXRoIHsNCgkNCglwdWJsaWMgZG91YmxlIGFkZChkb3VibGUgZmlyc3QsIGRvdWJsZSBzZWNvbmQpewkJDQoJCXJldHVybiBmaXJzdCtzZWNvbmQ7DQoJfQ0KfQ==\"}],\"tests\":[{\"packageName\":\"sample\",\"fileName\":\"MathTest.java\",\"content\":\"cGFja2FnZSBzYW1wbGU7DQoNCmltcG9ydCBvcmcuanVuaXQuVGVzdDsNCg0KaW1wb3J0IHN0YXRpYyBvcmcuanVuaXQuQXNzZXJ0Lio7DQoNCnB1YmxpYyBjbGFzcyBNYXRoVGVzdCB7DQoNCglwcml2YXRlIE1hdGggbWF0aCA9IG5ldyBNYXRoKCk7DQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCBzaG91bGRBZGROdW1iZXJzKCl7DQoJCS8vIGdpdmVuDQoJCWludCBhID0gMTA7DQoJCWRvdWJsZSBiID0gMi41Ow0KCQkNCgkJLy8gd2hlbg0KCQlkb3VibGUgcmVzdWx0ID0gbWF0aC5hZGQoYSwgYik7DQoJCQ0KCQkvLyB0aGVuDQoJCWFzc2VydEVxdWFscyhyZXN1bHQsIDEyLjUsIDAuMDAxKTsJCQ0KCX0JDQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCB0aGlzVGVzdFNob3VsZEZhaWxlZCgpew0KCQlhc3NlcnRGYWxzZSgiYmxhYmxhIix0cnVlKTsNCgl9DQp9DQo=\"}],\"buildGradle\":{\"content\":\"YXBwbHkgcGx1Z2luOiAnamF2YScNCg0Kc291cmNlQ29tcGF0aWJpbGl0eSA9IDEuNw0KdmVyc2lvbiA9ICcxLjAnDQoNCnJlcG9zaXRvcmllcyB7DQogICAgbWF2ZW5DZW50cmFsKCkNCn0NCg0KZGVwZW5kZW5jaWVzIHsNCg0KICAgIHRlc3RDb21waWxlIGdyb3VwOiAnanVuaXQnLCBuYW1lOiAnanVuaXQnLCB2ZXJzaW9uOiAnNC4xMicNCn0=\"},\"submissionId\":110}";
	private String brokenString = "{\"submissionId\": \"110\",\"classes\":[{\"packageName\": \"sample\",\"fileName\": \"Math.java\",\"content\": \"cGFja2FnZSBzYW1wbGU7DQoNCnB1YmxpYyBjbGFzcyBNYXRoIHsNCgkNCglwdWJsaWMgZG91YmxlIGFkZChkb3VibGUgZmlyc3QsIGRvdWJsZSBzZWNvbmQpewkJDQoJCXJldHVybiBmaXJzdCtzZWNvbmQ7DQoJfQ0KfQ==\"}],\"tests\":[{\"packageName\": \"sample\",\"fileName\": \"MathTest.java\",\"content\": \"cGFja2FnZSBzYW1wbGU7DQoNCmltcG9ydCBvcmcuanVuaXQuVGVzdDsNCg0KaW1wb3J0IHN0YXRpYyBvcmcuanVuaXQuQXNzZXJ0Lio7DQoNCnB1YmxpYyBjbGFzcyBNYXRoVGVzdCB7DQoNCglwcml2YXRlIE1hdGggbWF0aCA9IG5ldyBNYXRoKCk7DQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCBzaG91bGRBZGROdW1iZXJzKCl7DQoJCS8vIGdpdmVuDQoJCWludCBhID0gMTA7DQoJCWRvdWJsZSBiID0gMi41Ow0KCQkNCgkJLy8gd2hlbg0KCQlkb3VibGUgcmVzdWx0ID0gbWF0aC5hZGQoYSwgYik7DQoJCQ0KCQkvLyB0aGVuDQoJCWFzc2VydEVxdWFscyhyZXN1bHQsIDEyLjUsIDAuMDAxKTsJCQ0KCX0JDQoJDQoJQFRlc3QNCglwdWJsaWMgdm9pZCB0aGlzVGVzdFNob3VsZEZhaWxlZCgpew0KCQlhc3NlcnRGYWxzZSgiYmxhYmxhIix0cnVlKTsNCgl9DQp9DQo=\"}],\"buildGradle\": {\"content\": \"YXBwbHkgcGx1Z2luOiAnamF2YScNCg0Kc291cmNlQ29tcGF0aWJpbGl0eSA9IDEuNw0KdmVyc2lvbiA9ICcxLjAnDQoNCnJlcG9zaXRvcmllcyB7DQogICAgbWF2ZW5DZW50cmFsKCkNCn0NCg0KZGVwZW5kZW5jaWVzIHsNCg0KICAgIHRlc3RDb21waWxlIGdyb3VwOiAnanVuaXQnLCBuYW1lOiAnanVuaXQnLCB2ZXJzaW9uOiAnNC4xMicNCn0=\"}}";

	@Test
	public void shouldSetUpProjectData() throws InstantiationException, IllegalAccessException {
		// given
		JsonObject json = new JsonObject(jsonString);

		// when
		ProjectData projectData = JsonObjectParser.getInstance().convertJson(json, ProjectData.class);

		// then
		int submissionId = projectData.getSubmissionId();
		assertThat(submissionId, is(110));

		List<FileData> classes = projectData.getClasses();
		assertThat(classes.size(), is(1));

		FileData classFiles = classes.get(0);
		assertThat(classFiles.getContent(), notNullValue());
		assertThat(classFiles.getFileName(), is("Math.java"));
		assertThat(classFiles.getPackageName(), is("sample"));

		List<FileData> tests = projectData.getTests();
		assertThat(tests.size(), is(1));

		FileData testFiles = tests.get(0);
		assertThat(testFiles.getContent(), notNullValue());
		assertThat(testFiles.getFileName(), is("MathTest.java"));
		assertThat(testFiles.getPackageName(), is("sample"));

		String gradleContent = projectData.getBuildGradle().getContent();
		assertThat(gradleContent, notNullValue());
	}

	@Test(expected = JsonObjectParserException.class)
	public void shouldThrowException() throws IllegalAccessException, InstantiationException {
		// given
		JsonObject json = new JsonObject(brokenString);

		// when
		JsonObjectParser.getInstance().convertJson(json, ProjectData.class);
	}
}