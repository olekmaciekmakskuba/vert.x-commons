package pl.gda.pg.commons.json.testdata;

import java.util.List;

public class ProjectData {

	private List<FileData> classes;
	private List<FileData> tests;
	private GradleData buildGradle;
	private int submissionId;

	public List<FileData> getClasses() {
		return classes;
	}

	public void setClasses(List<FileData> classes) {
		this.classes = classes;
	}

	public List<FileData> getTests() {
		return tests;
	}

	public void setTests(List<FileData> tests) {
		this.tests = tests;
	}

	public GradleData getBuildGradle() {
		return buildGradle;
	}

	public void setBuildGradle(GradleData buildGradle) {
		this.buildGradle = buildGradle;
	}

	public int getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(int submissionId) {
		this.submissionId = submissionId;
	}
}
