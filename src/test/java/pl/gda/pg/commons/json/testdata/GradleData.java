package pl.gda.pg.commons.json.testdata;

public class GradleData {

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
